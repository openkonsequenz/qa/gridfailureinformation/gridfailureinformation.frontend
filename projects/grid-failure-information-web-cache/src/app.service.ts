/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as fs from 'fs-extra';

export interface ExternalGridFailure {
  source: string;
  password: string;
  failures: any[];
}

export interface ExternalSettings {
  password: string;
  settings: any;
}

export interface PublicGridFailure {
  postcode: string;
  freetextPostcode: string;
  latitude: string;
  longitude: string;
  failureBegin: string;
  failureEndPlanned: string;
  expectedReasonText: string;
  branch: string;
  city: string;
  district: string;
  addressPolygonPoints: string;
  radius: string;
}

@Injectable()
export class AppService {
  saveGridFailures(externalGridFailure: ExternalGridFailure, res: Function) {
    const jsonString = JSON.stringify(externalGridFailure.failures);
    return fs.writeFile('./store/public-sits.json', jsonString, err => {
      if (err) {
        throw err;
      } else {
        res(true, 'Successfully wrote file');
      }
    });
  }

  saveSettings(externalSettings: ExternalSettings, res: Function) {

    const jsonString = JSON.stringify(externalSettings.settings);
    return fs.writeFile('./store/public-settings.json', jsonString, err => {
      if (err) {
        throw err;
      } else {
        res(true, 'Successfully wrote file');
      }
    });
  }

  getAllSITs(res: Function) {
    if (process.env.FILTER_POSTCODE === 'true') {
      res(null);
      return;
    }
    fs.readFile('./store/public-sits.json', 'utf8', function(err, data) {
      if (err) throw err;
      else res(data); //do operation on data that generates say resultArray;
    });
  }

  getSITs(postcode: string, res: (data: any, error: string) => void) {

    if(!postcode) {
      this.getAllSITs(res);
      return;
    }
    const filePath = `./store/public-sits.json`;

    fs.readFile(filePath, 'utf8', function(err: NodeJS.ErrnoException | null, data: string) {
      if (err) {
        // Handle the error more gracefully, e.g., by calling res with an error message.
        res(null, `Error reading file for postcode ${postcode}: ${err.message}`);
      } else {
        try {

          //const gridFailures: PublicGridFailure[] = JSON.parse(data);
          const gridFailures: PublicGridFailure[] = JSON.parse(data);
          const filteredData = gridFailures.filter((failure: PublicGridFailure) => {
            return (
              (failure.postcode && failure.postcode === postcode) ||
              (failure.freetextPostcode && failure.freetextPostcode === postcode)
            );
          });

          // Map the filtered data to a new object containing only selected properties
          const mappedData = filteredData.map((failure: PublicGridFailure) => ({
            latitude: failure.latitude,
            longitude: failure.longitude,
            failureBegin: failure.failureBegin,
            failureEndPlanned: failure.failureEndPlanned,
            expectedReasonText: failure.expectedReasonText,
            branch: failure.branch,
            city: failure.city,
            district: failure.district,
            postcode: failure.postcode,
            freetextPostcode: failure.freetextPostcode,
            addressPolygonPoints: failure.addressPolygonPoints,
            radius: failure.radius,
            // Add other selected properties as needed
          }));


          res(mappedData, '');
        } catch (parseError) {
          // Handle parsing error
          res(null, `Error parsing file content: ${parseError.message}`);
        }
      }
    });
  }

  getSettings(res: (data: string) => void) {
    fs.readFile('./store/public-settings.json', 'utf8', function(err: NodeJS.ErrnoException | null, data: string) {
      if (err) throw err;
      else res(data); //do operation on data that generates say resultArray;
    });
  }
}
