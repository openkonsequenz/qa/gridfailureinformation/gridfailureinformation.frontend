/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '@grid-failure-information-app/shared/components/page-not-found/pageNotFound.component';
import { LogoutPageComponent } from '@grid-failure-information-app/pages/logout/logout/logout.component';
import { LoggedOutPageComponent } from '@grid-failure-information-app/pages/logout/logged-out/logged-out.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/grid-failures', pathMatch: 'full' },
  {
    path: 'distribution-groups',
    loadChildren: () => import('./pages/distribution-group/distribution-group.module').then(m => m.DistributionGroupModule),
  },
  {
    path: 'logout',
    component: LogoutPageComponent,
  },
  {
    path: 'loggedout',
    component: LoggedOutPageComponent,
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
