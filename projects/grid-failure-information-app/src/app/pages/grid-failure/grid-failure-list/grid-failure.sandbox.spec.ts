/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure.sandbox';
import { Store, ActionsSubject } from '@ngrx/store';
import { State } from '@grid-failure-information-app/shared/store';
import { of, Observable } from 'rxjs';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import { Router } from '@angular/router';
import { GridFailure } from '@grid-failure-information-app/shared/models/grid-failure.model';
import { FailureStation } from '@grid-failure-information-app/shared/models';

const FailureStations = [
  new FailureStation({ id: '1', stationId: '10', stationName: 'stationName1' }),
  new FailureStation({ id: '2', stationId: '20', stationName: 'stationName2' }),
  new FailureStation({ id: '3', stationId: '30', stationName: 'stationName3' }),
  new FailureStation({ id: '4', stationId: '40', stationName: 'stationName4' }),
];

describe('GridFailureSandbox', () => {
  let service: GridFailureSandbox;
  let appState: Store<State>;
  let router: Router;
  let actionsSubject: ActionsSubject;

  beforeEach(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionsSubject = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    router = { navigateByUrl() {} } as any;
    spyOn(appState, 'dispatch').and.callFake(() => {});

    service = new GridFailureSandbox(appState, router, actionsSubject);
  });

  it('should create GridFailureSandbox service', () => {
    expect(service).toBeTruthy();
  });

  it('should dispatch loadGridFailures Action via loadGridFailures()', () => {
    service.loadGridFailures();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailures());
  });

  it('should navigate to new route via createGridFailure()', () => {
    let spy = spyOn(router, 'navigateByUrl');
    service.createGridFailure();
    expect(spy).toHaveBeenCalledWith('/grid-failures/new');
  });

  it('should clear condensationList after calling clearGridFailureCondensation()', () => {
    service.condensationList = [new GridFailure()];
    service.clearGridFailureCondensation();
    expect(service.condensationList.length).toBe(0);
  });

  it('should add item to condensationList if its not already in them after calling addItemToCondensationList()', () => {
    service.condensationList = [];
    service.overviewGridFailureList = [new GridFailure()];
    service.addItemToCondensationList(new GridFailure());
    expect(service.condensationList.length).toBe(1);
    service.addItemToCondensationList(new GridFailure());
    expect(service.condensationList.length).toBe(1);
  });

  it('should remove item from condensationList after calling removeItemFromCondensationList()', () => {
    service.condensationList = [new GridFailure()];
    service.overviewGridFailureList = [new GridFailure()];
    service.removeItemFromCondensationList(new GridFailure());
    expect(service.condensationList.length).toBe(0);
  });

  it('should post condensationList after calling condenseCondensationList()', () => {
    service.condensationList = [new GridFailure()];
    service.condenseCondensationList();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.postGridFailuresCondensation({ payload: [new GridFailure().id] }));
  });

  it('should put condensationList after calling condenseCondensationList()', () => {
    service.condensationList = [new GridFailure()];
    service.condenseId = 'test';
    service.condenseCondensationList();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.putGridFailuresCondensation({ gridFailureId: 'test', payload: [new GridFailure().id] }));
  });

  it('should load condensedGridFailures and set id after calling loadCondensedGridFailures()', () => {
    service.loadCondensedGridFailures('');
    expect(service.condenseId).toBe('');
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadCondensedGridFailures({ payload: '' }));
  });

  it('should set gridFailureMapListset if FilteredGridFailureMapList was called', () => {
    const girdFailure = new GridFailure();
    service.setFilteredGridFailureMapList([girdFailure]);
    expect(service.filteredGridFailureMapList[0]).toBe(girdFailure);
  });

  it('should return comma seperated stationnames with id after calling _getStationDescription(..)', () => {
    const stationIds: string[] = ['1', '2'];
    const stationnames = (service as any)._getStationDescription(stationIds, FailureStations);
    expect(stationnames).toBeDefined();
    expect(stationnames).toContain(',');
    const stations = stationnames.split(',');
    expect(stations.length).toBe(2);
    expect(stations[0]).toContain('stationName1 (10)');
  });

  it('should return empty string after calling _getStationDescription(..) with unknown station ids ', () => {
    const stationIds: string[] = ['0'];
    const stationnames = (service as any)._getStationDescription(stationIds, FailureStations);
    expect(stationnames).toBeDefined();
    expect(stationnames).toBe('');
  });

  it('should return empty string after calling _getStationDescription(NULL) ', () => {
    const stationnames = (service as any)._getStationDescription(null, FailureStations);
    expect(stationnames).toBeDefined();
    expect(stationnames).toBe('');
  });

  it('should dispatch a "loadFailureReminder"-action', () => {
    (service as any)._startPolling();
    expectAsync((service as any)._timer.toPromise())
      .toBeResolved()
      .then(() => expect(appState.dispatch).toHaveBeenCalled());
  });
});
