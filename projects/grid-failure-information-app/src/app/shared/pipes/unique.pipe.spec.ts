/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { UniquePipe } from './unique.pipe';

describe('Pipe: Uniquee', () => {
  let uniquePipe: UniquePipe;

  // synchronous beforeEach
  beforeEach(() => {
    uniquePipe = new UniquePipe();
  });

  it('should be instanciated', () => {
    expect(uniquePipe).toBeDefined();
  });

  it('should return empty array if no items given', () => {
    const items = [];

    const filtered = uniquePipe.transform(items, 'filedNameText');

    expect(filtered.length).toBe(0);
    expect(filtered).toEqual([]);
  });

  it('should return empty array if no items given (2)', () => {
    const items = [];
    items.push({ branch: 'W', title: 'its a test title', statusId: '3', createUser: 'otto' });

    const filtered = uniquePipe.transform(items, '');

    expect(filtered.length).toBe(0);
  });

  it('should return empty array if items are undefined', () => {
    const items = undefined;
    const filtered = uniquePipe.transform(items, 'filedNameText');

    expect(filtered.length).toBe(0);
    expect(filtered).toEqual([]);
  });

  it('should return the whole input array if all items are defined and unique', () => {
    const items = [{ test: '1' }, { test: '2' }, { test: '3' }];
    const filtered = uniquePipe.transform(items, 'test');

    expect(filtered.length).toBe(3);
    expect(filtered).toEqual([{ test: '1' }, { test: '2' }, { test: '3' }]);
  });

  it('should return only the unique elements of the input array', () => {
    const items = [{ test: '1' }, { test: '2' }, { test: '3' }, { test: '3' }];
    const filtered = uniquePipe.transform(items, 'test');

    expect(filtered.length).toBe(3);
    expect(filtered).toEqual([{ test: '1' }, { test: '2' }, { test: '3' }]);
  });
});
