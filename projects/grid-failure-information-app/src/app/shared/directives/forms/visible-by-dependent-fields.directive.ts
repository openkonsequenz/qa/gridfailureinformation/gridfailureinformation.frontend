/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, Input, HostBinding, Host, Optional, ContentChild } from '@angular/core';
import * as store from '@grid-failure-information-app/shared/store';
import { Store } from '@ngrx/store';
import { DisableAction, EnableAction, ResetAction, SetValueAction, NgrxFormControlDirective, NgrxFormDirective } from 'ngrx-forms';

/**
 * This directive is used to hide or show html elemnts denping on defined dependencies  (isVisible = true)
 * Additionally it sets the state of depending field to avoid ngrx-forms validation errors.
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class FormControlVisibilityDirective
 */
@Directive({
  selector: '[visibleByDependentField]',
})
export class VisibleByDependentFieldDirective {
  private _isVisible: boolean = true;

  @Input()
  public set visibleByDependentField(isVisible: boolean) {
    this._isVisible = isVisible;
    if (!this.ngrxFormControl.state || !this.ngrxFormControl.state.id) {
      return;
    } else if (isVisible) {
      this._form.state.isEnabled && this._appState$.dispatch(new EnableAction(this.ngrxFormControl.state.id));
    } else {
      this._appState$.dispatch(new SetValueAction(this.ngrxFormControl.state.id, null));
      this._appState$.dispatch(new DisableAction(this.ngrxFormControl.state.id));
      this._appState$.dispatch(new ResetAction(this.ngrxFormControl.state.id));
    }
  }
  @ContentChild(NgrxFormControlDirective, { static: true, read: NgrxFormControlDirective })
  public ngrxFormControl: any;

  @HostBinding('hidden') get valid() {
    return !this._isVisible;
  }
  constructor(
    private _appState$: Store<store.State>,
    @Host()
    @Optional()
    private _form: NgrxFormDirective<any>
  ) {}
}
