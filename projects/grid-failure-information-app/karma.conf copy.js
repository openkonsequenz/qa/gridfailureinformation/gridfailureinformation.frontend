/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

if (!process.env.DISABLE_PUPPETEER) {
  process.env.CHROME_BIN = require('puppeteer').executablePath();
}
console.log('Chrome binary path:', process.env.CHROME_BIN);

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
      require('karma-junit-reporter'),
    ],
    client: {
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
      jasmine: {
        random: false,
      },
    },
    files: [
      { pattern: '../../config/*.json', watched: false, included: false, served: true, nocache: false },
      { pattern: '../../i18n/*.json', watched: false, included: false, served: true, nocache: false },
    ],
    proxies: {
      '../../config/': '/base/config/',
      '../../i18n/': '/base/i18n/',
    },
    preprocessors: {},
    mime: {
      'text/x-typescript': ['ts', 'tsx'],
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../../coverage/grid-failure-information-app'),
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true,
    },
    reporters: config.angularCli && config.angularCli.codeCoverage ? ['progress', 'coverage-istanbul'] : ['progress', 'kjhtml', 'dots', 'junit'],
    junitReporter: {
      outputDir: 'unittests',
      outputFile: 'test-results.xml',
    },

    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadlessNoSandbox'],
    usePolling: true,
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox', '--remote-debugging-port=9876', '--remote-debugging-address=0.0.0.0'], // , 'NO_PROXY=127.0.0.1',
      },
    },
    singleRun: false,
    concurrency: Infinity,
  });
};
