/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureDetailsSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.sandbox';
import { GridFailureDistributionGroupsComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-distribution-groups/grid-failure-distribution-groups.component';
import { DistributionGroup } from '@grid-failure-information-app/shared/models';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

describe('GridFailureDistributionGroupsComponent', () => {
  let component: GridFailureDistributionGroupsComponent;
  let sandbox: GridFailureDetailsSandbox;
  let translateService: TranslateService;

  beforeEach(() => {
    sandbox = {
      registerEvents() {},
      endSubscriptions() {},
      deleteDistributionGroupAssignment() {},
      showForDistStatus() {},
    } as any;
    translateService = { addLangs() {}, setDefaultLang() {}, use() {}, instant(l:any) {} } as any;
    component = new GridFailureDistributionGroupsComponent(sandbox, translateService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get/set selected distribution group', () => {
    component.selectedGroup = new DistributionGroup({ id: 'x' });
    expect(component.selectedGroup.id).toBe('x');
  });

  it('should call appropriate function for delete event', () => {
    const spy: any = spyOn(sandbox, 'deleteDistributionGroupAssignment');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'delete', data: new DistributionGroup() });
    expect(spy).toHaveBeenCalled();
  });

  it('should not call delete function for other (not delete) event', () => {
    const spy: any = spyOn(sandbox, 'deleteDistributionGroupAssignment');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'test', data: new DistributionGroup() });
    expect(spy).not.toHaveBeenCalled();
  });

  it('should clear selected distribution group', () => {
    component.selectedGroup = new DistributionGroup({ id: 'x' });
    component.clearSelectedGroup();
    expect(component.selectedGroup).toBeNull;
  });

  it('should unsubscribe OnDestroy', () => {
    component['_subscription'] = new Subscription();
    const spy: any = spyOn(component['_subscription'], 'unsubscribe');
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });
});
