/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { UtilService } from '@grid-failure-information-app/shared/utility';

describe('UtilService', () => {
  let component: UtilService;
  let translateService: any;
  let notificationService: any;
  let configService: any;

  beforeEach(async(() => {
    translateService = {
      instant() {},
    } as any;

    notificationService = {
      info: () => ({}),
      error: () => ({}),
      success: () => ({}),
      alert: () => ({}),
    } as any;

    configService = {
      get: () => ({ options: null }),
    } as any;
  }));

  beforeEach(() => {
    component = new UtilService(translateService, notificationService, configService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show info notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'info';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show error notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'error';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show success notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'success';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show alert notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'alert';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show notifications after call displayNotification without titleTranslationCode', () => {
    const messageTranslationCode = 'hello';
    const type = 'alert';
    const titleTranslationCode = null;
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should set the lookup name with the lookup code if exists and call instant an many times the length of the data', () => {
    const data = [
      { name: 'danny', code: '123' },
      { name: 'manny', code: '456' },
    ];
    const spyTranslation = spyOn(translateService, 'instant').and.returnValue({ '123': 'message 123', '456': 'message 456' });

    const res = component.translateLookupData(data);
    expect(spyTranslation).toHaveBeenCalledTimes(data.length);
    expect(res).toEqual([
      { name: 'message 123', code: '123' },
      { name: 'message 456', code: '456' },
    ]);
  });

  it('should not set the lookup name if code not exists and should not call instant', () => {
    const data = [{ name: 'danny' }, { name: 'manny' }];
    const spyTranslation = spyOn(translateService, 'instant').and.returnValue({ '123': 'message 123', '456': 'message 456' });

    const res = component.translateLookupData(data);
    expect(spyTranslation).not.toHaveBeenCalled();
    expect(res).toEqual(data);
  });
});
