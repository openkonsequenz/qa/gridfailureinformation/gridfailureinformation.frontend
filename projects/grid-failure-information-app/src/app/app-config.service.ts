/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { APP_BASE_HREF } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Settings } from '@grid-failure-information-app/shared/models';
import { map, catchError } from 'rxjs/operators';
import { InitialEmailContent } from '@grid-failure-information-app/shared/models/settings-initial-email-content.model';
import { of } from 'rxjs';

@Injectable()
export class ConfigService {
  private config: Object;
  private env: Object;

  constructor(private http: HttpClient, @Inject(APP_BASE_HREF) private baseHref: string) {}

  /**
   * Loads the environment config file first. Reads the environment variable from the file
   * and based on that loads the appropriate configuration file - development or production
   */
  load() {
    return new Promise((resolve, reject) => {
      this.http
        .get<any>(this.baseHref + 'config/env.json')
        .pipe(map(res => res))
        .subscribe(env_data => {
          this.env = env_data;

          this.http
            .get<any>(this.baseHref + 'config/' + env_data.env + '.json')
            .pipe(
              map(res =>res),
              catchError(error => of(error.json().error || 'Server error'))
            )
            .subscribe(data => {
              this.config = data;
              resolve(true);
            });
        });
    });
  }

  /**
   * Returns environment variable based on given key
   *
   * @param key
   */
  getEnv(key: any) {
    return this.env[key];
  }

  /**
   * Returns configuration value based on given key
   *
   * @param key
   */
  get(key: any) {
    return this.config[key];
  }

  static configAdapter(responseItem: any): Settings {
    return new Settings(responseItem);
  }
  static initialEmailContentAdapter(responseItem: any): InitialEmailContent {
    return new InitialEmailContent(responseItem);
  }
}
