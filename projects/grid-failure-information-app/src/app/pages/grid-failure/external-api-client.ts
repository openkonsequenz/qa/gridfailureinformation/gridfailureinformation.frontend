/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ExternalApiClient {
  constructor(private readonly _httpClient: HttpClient) {}

  public getNominatimData(nominatimUrl: string, longitude: number, latitude: number): Observable<any> {
    const url = `${nominatimUrl}/reverse?format=json&lon=${longitude}&lat=${latitude}&zoom=18&addressdetails=1`;
    return this._httpClient.get(url);
  }
}
