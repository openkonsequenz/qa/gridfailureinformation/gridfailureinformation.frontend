/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AutoResizeColumnsDirective } from '@grid-failure-information-app/app/shared/directives/agGrid/auto-resize-columns.directive';
import { Column } from 'ag-grid-community';

describe('AutoResizeColumnsDirective', () => {
  let component: AutoResizeColumnsDirective;
  let agGrid: any;
  let event: any;

  beforeEach(() => {
    event = {
      api: {
        sizeColumnsToFit() {},
        autoSizeColumns() {},
        setColumnWidth() {},
        getColumns() {},
      },
    };

    agGrid = {
      gridOptions: {
        event: event,
        suppressColumnVirtualisation: false,
        onPaginationChanged() {},
      },
    } as any;

    component = new AutoResizeColumnsDirective(agGrid);
  });

  it('should create an instance', () => {
    expect(component).toBeDefined();
  });

  it('should call _autoResizeColumns on init', () => {
    const spy = spyOn(component as any, '_autoResizeColumns');
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should change gridOptions when calling _autoResizeColumns', () => {
    (component as any)._autoResizeColumns();
    expect(agGrid.gridOptions.suppressColumnVirtualisation).toBeTruthy();
  });

  it('should not call getAllColumns if _gridWidth not defined and when calling _onGridViewRendered', () => {
    const spy = spyOn(event.api, 'getColumns');
    (component as any)._gridWidth = undefined;

    (component as any)._onGridViewRendered(event);
    expect(spy).not.toHaveBeenCalled();
  });

  it('should call many columnApi functions if _gridWidth defined and call setColumnWidth if _gridWidth is greater than the columns sum width', () => {
    const columns: Column[] = [new Column({}, {}, 'col', true), new Column({}, {}, 'col2', false)];
    const spy = spyOn(event.api, 'getColumns').and.returnValue(columns);
    const spyGetActualWidth0 = spyOn(columns[0], 'getActualWidth').and.returnValue(100);
    const spyGetActualWidth1 = spyOn(columns[1], 'getActualWidth').and.returnValue(200);
    const spy2 = spyOn(event.api, 'sizeColumnsToFit');
    const spy3 = spyOn(event.api, 'autoSizeColumns');
    const spySetColumnWidth = spyOn(event.api, 'setColumnWidth');

    (component as any)._gridWidth = 301;

    (component as any)._onGridViewRendered(event);
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(spy3).toHaveBeenCalled();
    expect(spyGetActualWidth0).toHaveBeenCalled();
    expect(spyGetActualWidth1).toHaveBeenCalled();
    expect(spySetColumnWidth).toHaveBeenCalledTimes(2);
  });

  it('should call many columnApi functions if _gridWidth defined but not call setColumnWidth if _gridWidth is less or equal than the columns sum width', () => {
    const columns: Column[] = [new Column({}, {}, 'col', true), new Column({}, {}, 'col2', false)];
    const spy = spyOn(event.api, 'getColumns').and.returnValue(columns);
    const spyGetActualWidth0 = spyOn(columns[0], 'getActualWidth').and.returnValue(100);
    const spyGetActualWidth1 = spyOn(columns[1], 'getActualWidth').and.returnValue(200);
    const spy2 = spyOn(event.api, 'sizeColumnsToFit');
    const spy3 = spyOn(event.api, 'autoSizeColumns');
    const spySetColumnWidth = spyOn(event.api, 'setColumnWidth');

    (component as any)._gridWidth = 299;

    (component as any)._onGridViewRendered(event);
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(spy3).toHaveBeenCalled();
    expect(spyGetActualWidth0).toHaveBeenCalled();
    expect(spyGetActualWidth1).toHaveBeenCalled();
    expect(spySetColumnWidth).not.toHaveBeenCalled();
  });
});
