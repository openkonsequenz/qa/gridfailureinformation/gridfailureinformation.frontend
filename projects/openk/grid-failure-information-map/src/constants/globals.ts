/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class Globals {
  static OVERVIEW_MAP_INITIAL_LATITUDE = 49.656634;
  static OVERVIEW_MAP_INITIAL_LONGITUDE = 8.423207;
  static OVERVIEW_MAP_INITIAL_ZOOM_FACTOR = 6;
  static DETAIL_MAP_INITIAL_ZOOM_FACTOR = 10;
  static MAX_ZOOM = 19;
  static ICON_SIZE_X_COORDINATE = 41;
  static ICON_SIZE_Y_COORDINATE = 51;
  static ICON_ANCHOR_X_COORDINATE = 20;
  static ICON_ANCHOR_Y_COORDINATE = 51;
  static MAP_ID = 'map';
  static TILE_ATTRIBUTION = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
  static TILE_LAYER = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  static RADIUS_BORDER_COLOR = '#204d74';
  static RADIUS_FILL_COLOR = '#337ab7';
  static RADIUS_FILL_OPACITY = 0.3;
  static STRONG_BEGIN_TAG = '<strong>';
  static STRONG_END_TAG = '</strong>';
  static GRID_FAILURE_BEGIN = 'Störungsbeginn:';
  static GRID_FAILURE_END_PLANNED = 'Voraussichtliches Ende:';
  static GRID_FAILURE_EXPECTED_REASON = 'Voraussichtlicher Grund:';
  static GRID_FAILURE_BRANCH = 'Sparte:';
  static GRID_FAILURE_ZIP = 'PLZ:';
  static GRID_FAILURE_CITY = 'Ort:';
  static GRID_FAILURE_DISTRICT = 'Ortsteil:';
  static BREAK_TAG = '<br/>';
  static MAP_ICON_URL ='./marker-icon.png'

  public static PROPERTIES_TO_BOX: string[] = ['addressPolygonPoints'];
}
