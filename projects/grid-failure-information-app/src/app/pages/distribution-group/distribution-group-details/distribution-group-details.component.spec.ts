/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { KeyValue } from '@angular/common';
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { DistributionGroupDetailsComponent } from '@grid-failure-information-app/pages/distribution-group/distribution-group-details/distribution-group-details.component';
import { DistributionPublicationStatusEnum, DistributionsGroupTextTypeEnum } from '@grid-failure-information-app/shared/constants/enums';

describe('DistributionGroupDetailsComponent', () => {
  let component: DistributionGroupDetailsComponent;
  let sandbox: DistributionGroupSandbox;

  beforeEach(() => {
    sandbox = {
      changeEmailTemplate: () => {},
    } as any;
    component = new DistributionGroupDetailsComponent(sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be able to compare key-value pairs', () => {
    let a: KeyValue<string, string> = { key: 'a', value: 'a' };
    let b: KeyValue<string, string> = { key: 'b', value: 'b' };
    expect(component.valueComperator(a, b)).toEqual(a.value.localeCompare(b.value));
  });

  it('should change the template selection from complete to publish after changeTemplate()', () => {
    sandbox.oldPublicationStatusForTemplate = DistributionPublicationStatusEnum.COMPLETE;
    const spy = spyOn(sandbox, 'changeEmailTemplate').and.callFake(() => {});
    component.changeTemplate(DistributionPublicationStatusEnum.PUBLISH, DistributionsGroupTextTypeEnum.Long);
    expect(spy).toHaveBeenCalled();
  });
});
