/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Globals } from '@openk-libs/grid-failure-information-map/constants/globals';
import { GridFailureMapInformation } from '@openk-libs/grid-failure-information-map/shared/models/grid-failure-coordinates.model';
import { MapOptions } from '@openk-libs/grid-failure-information-map/shared/models/map-options.model';
import { convertISOToLocalDateTime, determineDetailFieldVisibility } from '@openk-libs/grid-failure-information-map/shared/utility/utilityHelpers';
import * as L from 'leaflet';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'openk-grid-failure-information-map',
  templateUrl: './grid-failure-information-map.component.html',
  styleUrls: ['./grid-failure-information-map.component.scss'],
})
export class GridFailureInformationMapComponent implements OnDestroy {
  private _mapData: Array<GridFailureMapInformation> = [];
  private _mapDetailData: GridFailureMapInformation;
  private _interactionMode: boolean;
  private _mapOptions: MapOptions = new MapOptions();
  private _drawingsLayerGroup: L.LayerGroup = L.layerGroup();
  private _forceResizeSubscription: Subscription;
  private get _overviewMapInitialLatitude(): number {
    return +this._mapOptions.overviewMapInitialLatitude ? +this._mapOptions.overviewMapInitialLatitude : Globals.OVERVIEW_MAP_INITIAL_LATITUDE;
  }
  private get _overviewMapInitialLongitude(): number {
    return +this._mapOptions.overviewMapInitialLongitude ? +this._mapOptions.overviewMapInitialLongitude : Globals.OVERVIEW_MAP_INITIAL_LONGITUDE;
  }

  @Output() public gridFailureId: EventEmitter<string> = new EventEmitter();

  @Output() latLong: EventEmitter<any> = new EventEmitter();

  @Input() zoomIn = false;
  @Input() externalMap = false;
  @Input() customIcon = false;

  @Input() postcode$: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  @Input()
  set mapOptions(mapOptions: MapOptions) {
    if (!mapOptions) return;
    this._mapOptions = mapOptions;
    this._forceResizeSubscription = this._mapOptions.forceResize$.subscribe(() => {
      this.mapDetailData = this._mapDetailData;
    });
    this._initMap(this._overviewMapInitialLatitude, this._overviewMapInitialLongitude, this._mapOptions.detailMapInitialZoom, this._mapOptions);
  }

  @Input()
  public set mapData(data: Array<any>) {
    if (!data || !this._map) return;
    this._mapData = [];
    // Remove all popups from the map
    this._map.eachLayer(layer => {
      if (layer instanceof L.Popup) {
        this._map.removeLayer(layer);
      }
    });
    data.forEach(gridFailureData => {
      this._mapData.push(new GridFailureMapInformation(gridFailureData));
    });
    this._setMultipleMarkers();

    if (data.length === 0) {
      const center = this._map.getCenter();
      this.postcode$.subscribe((postcode: string) => {
        L.popup({ className: 'no-tip-popup', autoClose: true, closeOnClick: true, closeButton: false })
          .setLatLng(center)
          .setContent('Es liegen uns keine Störungsmeldungen für die PLZ ' + postcode + ' vor.')
          .openOn(this._map);
      });
    }
  }

  @Input()
  public set mapDetailData(gridFailureDetail: any) {
    this._mapDetailData = new GridFailureMapInformation(gridFailureDetail);
    this._setDetailMarker();
  }

  @Input()
  public set setInteractionMode(interactionMode: boolean) {
    this._interactionMode = interactionMode;
  }

  private _map: any;
  private _icon = L.icon({
    iconUrl: Globals.MAP_ICON_URL,
    iconSize: [Globals.ICON_SIZE_X_COORDINATE, Globals.ICON_SIZE_Y_COORDINATE],
    iconAnchor: [Globals.ICON_ANCHOR_X_COORDINATE, Globals.ICON_ANCHOR_Y_COORDINATE],
  });

  private _customIcon = L.divIcon({ className: 'custom-leaflet-div-icon', iconSize: [6, 6] });

  constructor() {}

  private _initMap(latitude: number, longitude: number, zoom: number, mapOptions: MapOptions): void {
    if (this._map) {
      return;
    }

    if (this.customIcon) {
      this._icon = this._customIcon;
    } else {
    if (!!mapOptions.iconUrl && mapOptions.iconUrl.trim().length) {
      this._icon = L.icon({
        iconUrl: mapOptions.iconUrl,
        iconSize: [Globals.ICON_SIZE_X_COORDINATE, Globals.ICON_SIZE_Y_COORDINATE],
        iconAnchor: [Globals.ICON_ANCHOR_X_COORDINATE, Globals.ICON_ANCHOR_Y_COORDINATE],
      });
    }
    }

    this._map = L.map(Globals.MAP_ID, {
      center: [latitude, longitude],
      zoom: zoom,
    });

    let tiles;
    if (!!mapOptions.mapWmsLayer) {
      tiles = L.tileLayer.wms(mapOptions.mapTileLayerUrl, {
        layers: mapOptions.mapWmsLayer,
        maxZoom: Globals.MAX_ZOOM,
        attribution: mapOptions.mapTileAttribution,
      });
    } else {
      tiles = L.tileLayer(mapOptions.mapTileLayerUrl, {
      maxZoom: Globals.MAX_ZOOM,
      attribution: mapOptions.mapTileAttribution,
        crossOrigin: null,
      });
    }

    tiles.on('load', () => {
      this._map.invalidateSize();
    });
    tiles.addTo(this._map);
    this._drawingsLayerGroup.addTo(this._map);

    this._map.on('click', this._emitLatLongValues.bind(this));
  }

  private _setMultipleMarkers(): void {
    if (!!this._map && !!this._mapData && this._mapOptions) {
      this._drawingsLayerGroup.clearLayers();
      const visibleMarkers = [];
      const gfis = [];


      this._mapData.forEach(gridFailure => {
        if (gridFailure.latitude && gridFailure.longitude) {
          const currentMarker = L.marker([gridFailure.latitude, gridFailure.longitude], { icon: this._icon })
            .addTo(this._drawingsLayerGroup)
            .on('click', () => this.gridFailureId.emit(gridFailure.id));
          let failureBeginVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'failureBegin');
          let failureBeginString = failureBeginVisibility
            ? `${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_BEGIN}${Globals.STRONG_END_TAG} ${
                !!gridFailure.failureBegin ? convertISOToLocalDateTime(gridFailure.failureBegin) : ''
              }${Globals.BREAK_TAG}`
            : '';

          let failureEndPlannedVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'failureEndPlanned');
          let failureEndPlannedString = failureEndPlannedVisibility
            ? ` ${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_END_PLANNED}${Globals.STRONG_END_TAG} ${
                !!gridFailure.failureEndPlanned ? convertISOToLocalDateTime(gridFailure.failureEndPlanned) : ''
              } ${Globals.BREAK_TAG}`
            : '';

          let expectedReasonVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'expectedReasonText');
          let expectedReasonString = expectedReasonVisibility
            ? `  ${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_EXPECTED_REASON}${Globals.STRONG_END_TAG} ${gridFailure.expectedReasonText || ''} ${
                Globals.BREAK_TAG
              }`
            : '';

          let branchVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'branch');
          let branchString = branchVisibility
            ? `  ${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_BRANCH}${Globals.STRONG_END_TAG} ${gridFailure.branchDescription || ''}`
            : '';

          /* adress fields: only if extendMarkerInformation is true */
          let postcodeVisibility =
            this._mapOptions.extendMarkerInformation &&
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'postcode');
          let postcodeString = postcodeVisibility
            ? `  ${Globals.BREAK_TAG}${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_ZIP}${Globals.STRONG_END_TAG} ${
                gridFailure.postcode || gridFailure.freetextPostcode || ''
              }`
            : '';
          let cityVisibility =
            this._mapOptions.extendMarkerInformation &&
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'city');
          let cityString = cityVisibility
            ? ` ${Globals.BREAK_TAG}${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_CITY}${Globals.STRONG_END_TAG} ${
                gridFailure.city || gridFailure.freetextCity || ''
              }`
            : '';

          let districtVisibility =
            this._mapOptions.extendMarkerInformation &&
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'district');
          let districtString = districtVisibility
            ? `  ${Globals.BREAK_TAG}${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_DISTRICT}${Globals.STRONG_END_TAG} ${
                gridFailure.district || gridFailure.freetextDistrict || ''
              }`
            : '';

          /* tooltip assembly */
          let tooltipContent = `${failureBeginString}${failureEndPlannedString}${expectedReasonString}${branchString}${postcodeString}${cityString}${districtString}`;
          currentMarker.bindTooltip(tooltipContent);
          if (!this.zoomIn) {
            this._drawPolygonOrCircle(gridFailure, tooltipContent);
          } else {
            gfis.push({ gridFailure, tooltipContent });
          }
          visibleMarkers.push(currentMarker);
        }
      });

      // Zoom to bounds of visible markers
      if (this.zoomIn && visibleMarkers.length > 0) {
        var group = new L.featureGroup(visibleMarkers);
        this._map.flyToBounds(group.getBounds().pad(0.093), {
          animate: true,
          maxZoom: 17,
        });
        this._map.once('moveend', () => {
          for (const gfi of gfis) {
            this._drawPolygonOrCircle(gfi.gridFailure, gfi.tooltipContent);
          }
        });
      }
    }
  }

  private _setDetailMarker(): void {
    if (!!this._map && !!this._mapDetailData) {
      if (this._mapDetailData.latitude && this._mapDetailData.longitude) {
        this._drawingsLayerGroup.clearLayers();
        L.marker([this._mapDetailData.latitude, this._mapDetailData.longitude], { icon: this._icon }).addTo(this._drawingsLayerGroup);
        this._drawPolygonOrCircle(this._mapDetailData, undefined);
        this._map.setView([this._mapDetailData.latitude, this._mapDetailData.longitude], this._map.getZoom());
      }
    }
  }

  // Draw whether polygon, circle or nothing; graded according to priority
  private _drawPolygonOrCircle(gridFailure: GridFailureMapInformation, tooltipContent: string): void {
    if (!!gridFailure.addressPolygonPoints && !!gridFailure.addressPolygonPoints['value'] && !!gridFailure.addressPolygonPoints['value'].length) {
      for (const polygonCoordinatesContainer of gridFailure.addressPolygonPoints['value']) {
        if (!!polygonCoordinatesContainer.polygonCoordinatesList && !!polygonCoordinatesContainer.polygonCoordinatesList.length) {
          L.polygon(polygonCoordinatesContainer.polygonCoordinatesList, {
            color: Globals.RADIUS_BORDER_COLOR,
            fillColor: Globals.RADIUS_FILL_COLOR,
            fillOpacity: Globals.RADIUS_FILL_OPACITY,
          }).addTo(this._drawingsLayerGroup);
        }
      }
    } else if (!!gridFailure.addressPolygonPoints && !!gridFailure.addressPolygonPoints.length) {
      for (const polygonCoordinatesContainer of gridFailure.addressPolygonPoints) {
        if (!!polygonCoordinatesContainer.polygonCoordinatesList && !!polygonCoordinatesContainer.polygonCoordinatesList.length) {
          L.polygon(polygonCoordinatesContainer.polygonCoordinatesList, {
            color: Globals.RADIUS_BORDER_COLOR,
            fillColor: Globals.RADIUS_FILL_COLOR,
            fillOpacity: Globals.RADIUS_FILL_OPACITY,
          }).addTo(this._drawingsLayerGroup);
        }
      }
    } else if (!!gridFailure.radius) {
      let circle = L.circle([gridFailure.latitude, gridFailure.longitude], {
        color: Globals.RADIUS_BORDER_COLOR,
        fillColor: Globals.RADIUS_FILL_COLOR,
        fillOpacity: Globals.RADIUS_FILL_OPACITY,
        radius: gridFailure.radius,
      }).addTo(this._drawingsLayerGroup);
      if (tooltipContent) {
        circle.bindTooltip(tooltipContent);
      }
    }
  }

  private _emitLatLongValues(event) {
    if (this._interactionMode) {
      const latLonValues = { latitude: event.latlng.lat, longitude: event.latlng.lng };
      this.latLong.emit(latLonValues);
    }
  }

  ngOnDestroy() {
    this._forceResizeSubscription.unsubscribe();
  }
}
