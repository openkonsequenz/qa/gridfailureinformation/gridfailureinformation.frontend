 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AnimationTriggerMetadata } from '@angular/animations';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

export const fadeInAnimation: AnimationTriggerMetadata = trigger(
  'fadeInAnimation',
  [
    state('true', style({ opacity: 1 })),
    state('false', style({ opacity: 0 })),
    transition('1 => 0', animate('100ms')),
    transition('0 => 1', animate('250ms'))
  ]
);
