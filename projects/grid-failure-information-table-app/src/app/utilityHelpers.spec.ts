/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as utilityHelpers from '@grid-failure-information-table-app/app/utilityHelpers';

describe('utilityHelpers', () => {
  it('should compare two dates and return 0 if equal', () => {
    const cellValue: string = '2020-04-20';
    const filterLocalDateAtMidnight: Date = new Date(2020, 3, 20);
    let testValue = utilityHelpers.dateTimeComparator(filterLocalDateAtMidnight, cellValue);
    expect(testValue).toBe(0);
  });

  it('should compare two dates and return -1 if cellDate less than filterLocalDateAtMidnight', () => {
    const cellValue: string = '2020-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnightString: string = '2021-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnight: Date = new Date(filterLocalDateAtMidnightString);
    let testValue = utilityHelpers.dateTimeComparator(filterLocalDateAtMidnight, cellValue);
    expect(testValue).toBe(-1);
  });

  it('should compare two dates and return 1 if cellDate greater than filterLocalDateAtMidnight', () => {
    const cellValue: string = '2021-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnightString: string = '2020-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnight: Date = new Date(filterLocalDateAtMidnightString);
    let testValue = utilityHelpers.dateTimeComparator(filterLocalDateAtMidnight, cellValue);
    expect(testValue).toBe(1);
  });

  it('should compare two dates and return -1 if cellValue is null', () => {
    const filterLocalDateAtMidnightString: string = '2021-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnight: Date = new Date(filterLocalDateAtMidnightString);
    let testValue = utilityHelpers.dateTimeComparator(filterLocalDateAtMidnight, null);
    expect(testValue).toBe(-1);
  });

  it('should compare two strings via stringInsensitiveComparator', () => {
    let testValue = utilityHelpers.stringInsensitiveComparator('A', 'b');
    expect(testValue).toBe(-1);
  });

  it('should handle null values in stringInsensitiveComparator ', () => {
    let testValue = utilityHelpers.stringInsensitiveComparator(null, null);
    expect(testValue).toBe(0);
  });
});
