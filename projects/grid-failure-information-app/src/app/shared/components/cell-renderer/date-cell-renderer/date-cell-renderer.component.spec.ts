/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DateCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';

describe('DateCellRendererComponent', () => {
  let component: DateCellRendererComponent;
  let fixture: ComponentFixture<DateCellRendererComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [DateCellRendererComponent],
    });

    fixture = TestBed.createComponent(DateCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
    expect(component).toBeTruthy();
  });

  it('agInit sets date to value property of its parameter', () => {
    component.agInit({ value: 'test' });
    expect(component.date).toEqual('test');
  });

  it('agInit sets date to value property of its parameter and sets isDate true if its a parsable number', () => {
    component.agInit({ value: '5' });
    expect(component.date).toEqual('5');
    expect(component.isDate).toBeTruthy();
  });

  it('refresh sets date to value property of its parameter', () => {
    const returnValue = component.refresh({ value: 'test' });
    expect(component.date).toEqual('test');
    expect(returnValue).toBe(true);
  });

  it('refresh sets date to value property of its parameter and sets isDate true if its a parsable number', () => {
    const returnValue = component.refresh({ value: '5' });
    expect(component.date).toEqual('5');
    expect(returnValue).toBe(true);
    expect(component.isDate).toBeTruthy();
  });
});
