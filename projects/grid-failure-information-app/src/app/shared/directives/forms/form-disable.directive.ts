/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, OnDestroy, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import * as store from '@grid-failure-information-app/shared/store';
import { Observable, Subject } from 'rxjs';
import { User } from '@grid-failure-information-app/shared/models/user';
import { PermissionsModel } from '@grid-failure-information-app/shared/models/permissions.model';
import { takeUntil, take, map } from 'rxjs/operators';
import { FormState } from 'ngrx-forms';
import { DisableAction } from 'ngrx-forms';
import { StateEnum } from '@grid-failure-information-app/shared/constants/enums';

/**
 * This directive disables the entire form depending on user rights
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class FormDisableDirective
 * @implements {OnDestroy}
 */
@Directive({
  selector: 'form[ngrxFormState]',
})
export class FormDisableDirective implements OnDestroy {
  @Input()
  public set ngrxFormState(formState: FormState<any>) {
    if (!!formState && formState.isDisabled) {
      return;
    }
    this._permissions$.pipe(take(1), takeUntil(this._endSubscriptions$)).subscribe(permissions => {
      if (
        !!permissions.reader ||
        (!permissions.qualifier && !permissions.creator && formState.value.statusIntern === StateEnum.QUALIFIED) ||
        formState.value.statusIntern === StateEnum.COMPLETED ||
        formState.value.statusIntern === StateEnum.CANCELED
      ) {
        this._appState$.dispatch(new DisableAction(formState.id));
      }
    });
  }

  private _endSubscriptions$: Subject<boolean> = new Subject();

  private _permissions$: Observable<PermissionsModel> = this._appState$
    .select(store.getUser)
    .pipe(
      takeUntil(this._endSubscriptions$),
      map((user: User) => new PermissionsModel(user.roles))
    )

  constructor(private _appState$: Store<store.State>) {}

  ngOnDestroy() {
    this._endSubscriptions$.next(true);
  }
}
