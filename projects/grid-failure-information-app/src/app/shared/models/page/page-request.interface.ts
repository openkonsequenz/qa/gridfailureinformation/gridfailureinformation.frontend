 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export interface PageRequestInterface {

  /**
   * Used to define any query paramter
   *
   * @type {*}
   * @memberof PageRequestInterface
   */
  queryParameter?: any;

  /**
   * The index number of the page requested.
   *
   * @type {number}
   * @memberof PageRequestInterface
   */
  pageNumber?: number;
  /**
   * The number of items present on a single page.
   *
   * @type {number}
   * @memberof PageRequestInterface
   */
  pageSize?: number;
  /**
   * The number of items to skip when determining the item that the first page starts with.
   *
   * @type {number}
   * @memberof PageRequestInterface
   */
  offset?: number;

  /**
   * Should the result be paged?
   *
   * @type {boolean}
   * @memberof PageRequestInterface
   */
  paged?: boolean;
}