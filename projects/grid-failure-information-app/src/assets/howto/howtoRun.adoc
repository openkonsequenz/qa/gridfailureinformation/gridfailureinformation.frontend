= openKonsequenz - How to run the module "eLogbook@openK"
:Date: 2017-11-17
:Revision: 1
:icons:
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

<<<

IMPORTANT: Please be sure that you have first *Portal (Auth n Auth)* installed and configured!

== Requirements
* Browser (Chrome or IE11 suggested)

== Prerequisites

* *To see this application running you have to run Portal application too.* The reason is the authentication, which happened in the Portal login phase.

* A developing and administrating software for databases

** To work with a postgreSQL database is pgAdmin suggested

*** Download and install pgAdmin (version 3 is used during developing process) from:
        https://www.pgadmin.org/download/
*** Create a database and adapt the `<tomcat>/conf/`*context.xml* file to your database (look at *elogbook_howtoBuild*).

*** To initialize the database schema run the sql scripts:

 /db/postgreSQL/02_create_DB_1.0.0.sql
 /db/postgreSQL/03_config_DB_1.0.0.sql


TIP: For detailed instructions look at *http://87.128.212.254:8880/elogbookFE_ADoc/elogbook_architectureDocumentation.html[elogbook_architectureDocumentation]*

== How to run the Backend
To run the backend you need to have installed and configured Apache Tomcat (look at *elogbook_howtoBuild*).

=== Set up and start Apache Tomcat
Tomcat needs the Web Application Archive (war) file, which is produced by building the maven project, to run the application.

* Copy the *betriebstagebuch.war* file from the project file `/target` in the `<tomcat>/webapps` file. If there is a folder named *betriebstagebuch*, delete it before.
* Navigate to `C:\apache-tomcat-8.0.30\bin` and start Tomcat by clicking on *startup.bat*.
* Tomcat's default port is *8080*.

[source,text]
----
If 8080 port is already in use, you have to change tomcat's port as follows:
- Go to <tomcat>/conf folder
- Open server.xml file and search for "Connector port"
- Replace "8080" by your port number (for example 8181)
- Restart tomcat server (in the same folder which startup.bat is located, you can also find a shutdown.bat file).
----
{blank}

IMPORTANT: If you change the part number you have also adapt the port in the
frontend project: File "<PRJ_elogbookFE>/proxy.conf.json"

TIP: Look at the *http://87.128.212.254:8880/elogbookFE_ADoc/elogbook_interfaceDocumentation.html[elogbook_interfaceDocumentation]* for informations about the services.

== How to run the Frontend
To run the frontend project you need to have installed and updated Node.js and npm Angular-CLI.

=== Compile the Frontend

To compile say Angular-CLI to start.

* Open a command line and navigate to the root folder of the frontend project
* Run the command

[source,command]
----
   $  npm start
----
{blank}

* Open a browser and type:

[source,http]
----
    http://localhost:4200
----
{blank}

IMPORTANT: The reason you maybe don´t see the application running properly, is that you have not even logged in.
To do so, run first the Portal project, where you can log in and then open the elogbook.



